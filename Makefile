proto:
	echo -e "Generating Go GRPC Code"
	protoc --go_out=. --go_opt=paths=source_relative pb/eventstore.proto

test:
	echo "Executing tests"

build:
	echo "Compiling for every OS and Platform"
	GOOS=linux CGO_ENABLED=0 go build -o bin/srv_linux cmd/srv/main.go
	GOOS=darwin GOARCH=amd64 CGO_ENABLED=0 go build -o bin/srv_mac cmd/srv/main.go

all: proto test build
